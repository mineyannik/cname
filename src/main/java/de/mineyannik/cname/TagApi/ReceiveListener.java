package de.mineyannik.cname.TagApi;

import de.mineyannik.cname.Main;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.kitteh.tag.AsyncPlayerReceiveNameTagEvent;

public class ReceiveListener implements Listener {
    private final Main plugin;

    public ReceiveListener(Main plugin)
    {
        this.plugin = plugin;
    }

    @EventHandler
    public void onNameTag(AsyncPlayerReceiveNameTagEvent e)
    {
        Player p = e.getNamedPlayer();
        if (!this.plugin.cname.containsKey(p.getUniqueId())) {
            return;
        }
        String tag = (String)this.plugin.cname.get(p.getUniqueId());

        e.setTag(tag);
    }
    
    @EventHandler(priority = EventPriority.MONITOR)
    public void onLogout(PlayerQuitEvent e)
    {
        if (this.plugin.cname.containsKey(e.getPlayer().getUniqueId()))
        {
            this.plugin.cname.remove(e.getPlayer().getUniqueId());
        }
    }
}