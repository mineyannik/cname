package de.mineyannik.cname;

import de.mineyannik.cname.TagApi.ReceiveListener;
import de.mineyannik.cname.kommandos.CNAME;
import java.io.IOException;
import java.util.HashMap;
import java.util.UUID;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.mcstats.MetricsLite;

public class Main extends JavaPlugin {
    public HashMap<UUID, String> cname = new HashMap<UUID, String>();

    @Override
    public void onEnable()
    {
        //Plugin deaktivieren falls TAGAPI nicht vorhanden
        if (!(Bukkit.getPluginManager().isPluginEnabled("TagAPI")))
        {
            System.err.println("TagAPI not Found.");
            Bukkit.getPluginManager().disablePlugin(this);
        }
        
        //Plugin Metrics
        try {
            MetricsLite metrics = new MetricsLite(this);
            metrics.start();
        } catch (IOException e) {
         System.out.println("Failed to send stats to Plugin Metrics: " + e.getMessage());
        }
        this.Kommandos();
        this.Listeners();
        this.config();
    }

    @Override
    public void onDisable()
    {
        this.cname.clear();
        for (Player p : getServer().getOnlinePlayers())
        {
            p.setPlayerListName(p.getName());
            p.setDisplayName(p.getName());
        }       
    }

    public void Kommandos()
    {
        this.getCommand("cname").setExecutor(new CNAME(this));

	this.getCommand("nick").setExecutor(new CNAME(this));
    }

    public void Listeners()
    {
        PluginManager pm = getServer().getPluginManager();

        pm.registerEvents(new ReceiveListener(this), this);
    }

    public void config()
    {   
        FileConfiguration config = this.getConfig();

        config.addDefault("messages.onenable", "You now have a Fake Name!");
        config.addDefault("messages.ondisable", "You now have no more a Fake Name!");
        config.addDefault("messages.nopermissions", "You don't have Permissions to do that!");
        config.addDefault("messages.commandsenderisnoplayer", "This Comand can only be executed by a player");

        config.options().copyDefaults(true);
        this.saveConfig();
    }
}
/*
Changelog
    1.4.4
        - Updated to new UDID System.
    1.4.3
        - Updatet to new Bukkit Version.
        - Little Color Bugfix
    1.4.2:
        - TagAPI Bugfix and Update
        - MoreBugfixes
    1.4.1:
        - Added Plugin Metrics
        - Code Optimations
        - Added Message if CommandSender is not a Player.
        - Bug Fixes
    1.4.2:
        - Code Optimations
*/