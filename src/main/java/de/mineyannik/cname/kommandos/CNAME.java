package de.mineyannik.cname.kommandos;

import de.mineyannik.cname.Main;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.kitteh.tag.TagAPI;

public class CNAME implements CommandExecutor {
    private final Main plugin;

    public CNAME(Main plugin)
    {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender arg0, Command arg1, String arg2, String[] arg3)
    {
        if (!(arg0 instanceof Player))
        {
            arg0.sendMessage(this.plugin.getConfig().getString("messages.commandsenderisnoplayer"));
            return true;
        }
    
        Player p = (Player)arg0;

        FileConfiguration config = this.plugin.getConfig();
        if (arg3.length == 1)
        {
            if (arg0.hasPermission("cname.use"))
            {
                p.setDisplayName(arg3[0] + ChatColor.RESET);

                this.plugin.cname.put(p.getUniqueId(), arg3[0]);

                p.sendMessage(ChatColor.GREEN + config.getString("messages.onenable"));
                p.setPlayerListName(arg3[0]);


                TagAPI.refreshPlayer(p);

                return true;
            }
            p.sendMessage(ChatColor.DARK_RED + config.getString("messages.nopermissions"));
            return true;
        }
        if (this.plugin.cname.containsKey(p.getUniqueId()))
        {
            p.setDisplayName(p.getName());
            this.plugin.cname.remove(p.getUniqueId());

            p.setPlayerListName(p.getName());

            p.sendMessage(ChatColor.GREEN + config.getString("messages.ondisable"));

            TagAPI.refreshPlayer(p);

            return true;
        }
        return false;
    }
}